#!/usr/bin/env bash
[[ $# -eq 1 ]] || exit 1
version=$1
scp foo_wave_minibar-${version}-archive.fb2k-component foo_wave_minibar-${version}.fb2k-component zao@lenin.acc.umu.se:public_html/

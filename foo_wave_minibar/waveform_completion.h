﻿#pragma once

#include "analysis.h"
#include <SDK/foobar2000.h>

struct waveform_callback
{
  virtual void on_current_waveform(std::shared_ptr<waveform_entry> wf,
                                   metadb_handle_ptr track,
                                   float percentage)
  {
  }
  virtual void on_background_waveform(std::shared_ptr<waveform_entry> wf,
                                      metadb_handle_ptr track)
  {
  }
};

struct waveform_callback_manager : service_base
{
  virtual void register_callback(waveform_callback* cb) = 0;
  virtual void unregister_callback(waveform_callback* cb) = 0;

  virtual void dispatch_current_waveform(std::shared_ptr<waveform_entry> wf,
    metadb_handle_ptr track,
    float percentage) = 0;

  virtual void dispatch_background_waveform(std::shared_ptr<waveform_entry> wf,
    metadb_handle_ptr track) = 0;

  FB2K_MAKE_SERVICE_INTERFACE_ENTRYPOINT(waveform_callback_manager);
};

// {A064E42E-263B-4A7D-B1D9-5F301BF83992}
FOOGUIDDECL GUID const waveform_callback_manager::class_guid = {
  0xa064e42e,
  0x263b,
  0x4a7d,
  { 0xb1, 0xd9, 0x5f, 0x30, 0x1b, 0xf8, 0x39, 0x92 }
};
#include <ATLHelpers/ATLHelpers.h>
#include <columns_ui-sdk/ui_extension.h>

#include <stdlib.h>

#include "sqlite3.h"
#include "miniz.h"

#include <atlbase.h>
#include <atlwin.h>

#include <algorithm>
#include <atomic>
#include <array>
#include <memory>
#include <mutex>
#include <optional>
#include <sstream>
#include <thread>

#include "analysis.h"
#include "render.h"
#include "seek_tooltip.h"
#include "waveform_completion.h"

DECLARE_COMPONENT_VERSION("Waveform Minibar", "1.26", "zao");
VALIDATE_COMPONENT_FILENAME("foo_wave_minibar.dll");

sqlite3* db_profile;
sqlite3* db_memory;

std::recursive_mutex shutdown_mutex;
std::atomic<bool> shutdown_flag = false;

template<typename F>
void
unless_terminating(F&& f)
{
  std::unique_lock<std::recursive_mutex> lk(shutdown_mutex);
  if (!shutdown_flag) {
    f();
  }
}

CRITICAL_SECTION scan_mutex;
pfc::string8 current_name;
metadb_handle_ptr current_track;

struct scan_handle
{
  abort_callback_impl abort_cb;
};

std::shared_ptr<scan_handle> queued_action_scan;

struct mutex_initer
{
  mutex_initer() { InitializeCriticalSection(&scan_mutex); }
} mutex_initer;

static std::wstring
to_utf16(std::string const& src)
{
  DWORD cch =
    MultiByteToWideChar(CP_UTF8, 0, src.c_str(), src.size() + 1, nullptr, 0);
  wchar_t* buf = new wchar_t[cch];
  MultiByteToWideChar(CP_UTF8, 0, src.c_str(), src.size() + 1, buf, cch);
  std::wstring dst = buf;
  delete[] buf;
  return dst;
}

struct initquit_db : initquit
{
  void on_init() override
  {
    std::string ppath = core_api::get_profile_path();
    {
      std::string filename = ppath.substr(7) + "\\minicache.db";
      sqlite3_open_v2(filename.c_str(),
                      &db_profile,
                      SQLITE_OPEN_CREATE | SQLITE_OPEN_READWRITE,
                      nullptr);
      sqlite3_open_v2(":memory:",
                      &db_memory,
                      SQLITE_OPEN_CREATE | SQLITE_OPEN_READWRITE,
                      nullptr);
      sqlite3* dbs[] = { db_profile, db_memory };
      for (auto db : dbs) {
        sqlite3_exec(db,
                     "CREATE TABLE IF NOT EXISTS waveform_v1 "
                     "(track_id TEXT NOT NULL PRIMARY KEY"
                     ",true_size INTEGER NOT NULL"
                     ",payload BLOB NOT NULL);",
                     nullptr,
                     nullptr,
                     nullptr);
      }
    }
  }

  void on_quit() override
  {
    std::unique_lock<std::recursive_mutex> lk(shutdown_mutex);
    shutdown_flag = true;

    sqlite3_close_v2(db_memory);
    sqlite3_close_v2(db_profile);
    db_memory = nullptr;
    db_profile = nullptr;
    sqlite3_shutdown();
  }
};

static initquit_factory_t<initquit_db> g_iqdb;

// {1866C3C0-9109-4789-881A-D6A94804F995}
static GUID const g_guid_branch_id = {
  0x1866c3c0,
  0x9109,
  0x4789,
  { 0x88, 0x1a, 0xd6, 0xa9, 0x48, 0x4, 0xf9, 0x95 }
};

static advconfig_branch_factory g_advcfg_branch(
  "Waveform Minibar",
  g_guid_branch_id,
  advconfig_entry::guid_branch_display,
  0.0);

// {E91F1194-E758-4C1D-A5FF-DC5380801DEA}
static GUID g_guid_format_track_string = {
  0xe91f1194,
  0xe758,
  0x4c1d,
  { 0xa5, 0xff, 0xdc, 0x53, 0x80, 0x80, 0x1d, 0xea }
};

static advconfig_string_factory g_format_track_string(
  "Equality format",
  g_guid_format_track_string,
  g_guid_branch_id,
  0.0,
  "$lower([%album "
  "artist%]|[%album%]|[$year(%date%)]|[$num(%discnumber%,1).][$num(%"
  "tracknumber%,2)]|%title%|[%length%])");

// {0C4D8A02-28A2-46FF-A1CF-14592B33AA89}
static GUID const g_guid_branch_unplayed_colors = {
  0xc4d8a02,
  0x28a2,
  0x46ff,
  { 0xa1, 0xcf, 0x14, 0x59, 0x2b, 0x33, 0xaa, 0x89 }
};

static advconfig_branch_factory g_unplayed_branch("Unplayed colors",
                                                  g_guid_branch_unplayed_colors,
                                                  g_guid_branch_id,
                                                  0.1f);

// {9B38D936-2FB4-47AC-A165-0159C53A237E}
static GUID const g_guid_unplayed_background = {
  0x9b38d936,
  0x2fb4,
  0x47ac,
  { 0xa1, 0x65, 0x1, 0x59, 0xc5, 0x3a, 0x23, 0x7e }
};

// {08A18B8F-AAA1-4A64-A4B5-209F62FD8BF5}
static GUID const g_guid_unplayed_fill = {
  0x8a18b8f,
  0xaaa1,
  0x4a64,
  { 0xa4, 0xb5, 0x20, 0x9f, 0x62, 0xfd, 0x8b, 0xf5 }
};

// {BAA4BE45-4A2D-44AB-AA22-1EF61A7CEDB8}
static GUID const g_guid_unplayed_rms = {
  0xbaa4be45,
  0x4a2d,
  0x44ab,
  { 0xaa, 0x22, 0x1e, 0xf6, 0x1a, 0x7c, 0xed, 0xb8 }
};

// {37D5B4A4-9E1C-48C8-A73F-F15B9BA438A9}
static GUID const g_guid_branch_played_colors = {
  0x37d5b4a4,
  0x9e1c,
  0x48c8,
  { 0xa7, 0x3f, 0xf1, 0x5b, 0x9b, 0xa4, 0x38, 0xa9 }
};

static advconfig_string_factory g_unplayed_background(
  "Background",
  g_guid_unplayed_background,
  g_guid_branch_unplayed_colors,
  0.0,
  "FFFFFF");
static advconfig_string_factory g_unplayed_fill("Fill",
                                                g_guid_unplayed_fill,
                                                g_guid_branch_unplayed_colors,
                                                0.0,
                                                "6B6B6B");
static advconfig_string_factory g_unplayed_rms("RMS",
                                               g_guid_unplayed_rms,
                                               g_guid_branch_unplayed_colors,
                                               0.0,
                                               "B5B5B5");

static advconfig_branch_factory g_played_branch("Played colors",
                                                g_guid_branch_played_colors,
                                                g_guid_branch_id,
                                                0.2f);

// {CF00845F-8232-47C5-BD5A-56C623224F51}
static GUID const g_guid_played_background = {
  0xcf00845f,
  0x8232,
  0x47c5,
  { 0xbd, 0x5a, 0x56, 0xc6, 0x23, 0x22, 0x4f, 0x51 }
};

// {01A272F0-125F-492F-9D22-84EF15C30BCC}
static GUID const g_guid_played_fill = {
  0x1a272f0,
  0x125f,
  0x492f,
  { 0x9d, 0x22, 0x84, 0xef, 0x15, 0xc3, 0xb, 0xcc }
};

// {4D06557C-5813-4474-897C-C1A54E69A696}
static GUID const g_guid_played_rms = {
  0x4d06557c,
  0x5813,
  0x4474,
  { 0x89, 0x7c, 0xc1, 0xa5, 0x4e, 0x69, 0xa6, 0x96 }
};

static advconfig_string_factory g_played_background("Background",
                                                    g_guid_played_background,
                                                    g_guid_branch_played_colors,
                                                    0.0,
                                                    "FFFFFF");
static advconfig_string_factory g_played_fill("Fill",
                                              g_guid_played_fill,
                                              g_guid_branch_played_colors,
                                              0.0,
                                              "0060B8");
static advconfig_string_factory g_played_rms("RMS",
                                             g_guid_played_rms,
                                             g_guid_branch_played_colors,
                                             0.0,
                                             "7FAFDB");

// {7234083C-C922-4532-9B13-91686604C716}
static GUID const g_guid_show_window_edge = {
  0x7234083c,
  0xc922,
  0x4532,
  { 0x9b, 0x13, 0x91, 0x68, 0x66, 0x4, 0xc7, 0x16 }
};

static advconfig_checkbox_factory g_show_window_edge("Show window border",
                                                     g_guid_show_window_edge,
                                                     g_guid_branch_id,
                                                     0.0,
                                                     true);

struct track_formatter
{
  track_formatter()
  {
    pfc::string8 format;
    g_format_track_string.get(format);
    static_api_ptr_t<titleformat_compiler>()->compile_safe_ex(
      to, format.c_str(), "%path% | %subsong%");
  }

  pfc::string8 format_track(metadb_handle_ptr track) const
  {
    pfc::string8 ret;
    file_info_impl fi;
    track->get_info(fi);
    to->run_simple(track->get_location(), &fi, ret);
    return ret;
  }

  service_ptr_t<titleformat_object> to;
};

static pfc::string8
format_track(metadb_handle_ptr track)
{
  track_formatter tf;
  return tf.format_track(track);
}

struct stored_waveform_view_v1
{
  uint16_t sample_count;
  void* min_samples;
  void* max_samples;
  void* rms_samples;
};

std::optional<stored_waveform_view_v1>
parse_stored_waveform_view_v1(void* stored_data, uint32_t stored_size)
{
  stored_waveform_view_v1 view{};
  auto p = (uint8_t*)stored_data;
  auto n = stored_size;
  if (n >= sizeof(uint16_t)) {
    uint16_t version;
    memcpy(&version, p, sizeof(uint16_t));
    p += sizeof(uint16_t);
    n -= sizeof(uint16_t);
    if (version == 1) {
      if (n >= sizeof(uint16_t)) {
        memcpy(&view.sample_count, p, sizeof(uint16_t));
        p += sizeof(uint16_t);
        n -= sizeof(uint16_t);
        size_t min_bytes = view.sample_count * sizeof(int16_t);
        size_t max_bytes = view.sample_count * sizeof(int16_t);
        size_t rms_bytes = view.sample_count * sizeof(uint16_t);
        if (n == min_bytes + max_bytes + rms_bytes) {
          view.min_samples = p;
          p += min_bytes;
          n -= min_bytes;
          view.max_samples = p;
          p += max_bytes;
          n -= max_bytes;
          view.rms_samples = p;
          p += rms_bytes;
          n -= max_bytes;
          return view;
        }
      }
    }
  }
  return {};
}

std::vector<uint8_t>
encode_waveform_v1(waveform_entry const& wf)
{
  size_t min_bytes = wf.sample_count * sizeof(int16_t);
  size_t max_bytes = wf.sample_count * sizeof(int16_t);
  size_t rms_bytes = wf.sample_count * sizeof(uint16_t);
  assert(wf.min_samples.size() == wf.sample_count);
  assert(wf.max_samples.size() == wf.sample_count);
  assert(wf.rms_samples.size() == wf.sample_count);
  std::vector<uint8_t> ret(sizeof(uint16_t) + sizeof(uint16_t) + min_bytes +
                           max_bytes + rms_bytes);
  uint16_t version = 1;
  uint16_t sample_count = wf.sample_count;
  auto p = ret.data();
  memcpy(p, &version, sizeof(uint16_t));
  p += sizeof(uint16_t);
  memcpy(p, &sample_count, sizeof(uint16_t));
  p += sizeof(uint16_t);
  memcpy(p, wf.min_samples.data(), min_bytes);
  p += min_bytes;
  memcpy(p, wf.max_samples.data(), max_bytes);
  p += max_bytes;
  memcpy(p, wf.rms_samples.data(), rms_bytes);
  p += rms_bytes;
  return ret;
}

static std::shared_ptr<waveform_entry>
find_waveform(metadb_handle_ptr track)
{
  core_api::ensure_main_thread();
  std::shared_ptr<waveform_entry> wf;
  pfc::string8 track_id = format_track(track);
  sqlite3* dbs[] = { db_memory, db_profile };
  for (auto db : dbs) {
    if (!db) {
      continue;
    }
    sqlite3_stmt* stmt = {};
    sqlite3_prepare_v2(
      db,
      "SELECT true_size, payload FROM waveform_v1 WHERE track_id=?;",
      -1,
      &stmt,
      nullptr);
    sqlite3_bind_text(stmt, 1, track_id.c_str(), -1, SQLITE_STATIC);
    if (sqlite3_step(stmt) == SQLITE_ROW) {
      size_t true_size = (size_t)sqlite3_column_int(stmt, 0);
      size_t packed_size = sqlite3_column_bytes(stmt, 1);
      void const* packed_data = sqlite3_column_blob(stmt, 1);
      auto data = std::make_unique<uint8_t[]>(true_size);
      mz_ulong bytes_uncompressed = true_size;
      if (MZ_OK == mz_uncompress(data.get(),
                                 &bytes_uncompressed,
                                 (uint8_t const*)packed_data,
                                 (mz_ulong)packed_size)) {
        if (auto owf = parse_stored_waveform_view_v1(data.get(), true_size)) {
          wf = std::make_shared<waveform_entry>(owf->sample_count);
          size_t byte_count = owf->sample_count * sizeof(uint16_t);
          memcpy(wf->min_samples.data(), owf->min_samples, byte_count);
          memcpy(wf->max_samples.data(), owf->max_samples, byte_count);
          memcpy(wf->rms_samples.data(), owf->rms_samples, byte_count);
        }
      }
    }
    sqlite3_finalize(stmt);
    if (wf) {
      break;
    }
  }

  return wf;
}

void
g_waveform_ready(std::shared_ptr<waveform_entry> wf,
                 metadb_handle_ptr track,
                 float percentage)
{
  unless_terminating([&] {
    static_api_ptr_t<waveform_callback_manager> wcm;
    wcm->dispatch_current_waveform(wf, track, percentage);
  });
}

bool
track_in_library_main_only(metadb_handle_ptr track)
{
  static_api_ptr_t<library_manager> lm;
  return lm->is_item_in_library(track);
}

struct rate_limit
{
  uint64_t tick_freq;
  uint64_t min_tick_count_between_events;
  uint64_t last_tick_count;
  size_t event_count;
  size_t event_limit;

  rate_limit(size_t event_limit, float min_time_between_events)
    : event_count(0)
    , event_limit(event_limit)
  {
    QueryPerformanceFrequency((LARGE_INTEGER*)&tick_freq);
    min_tick_count_between_events =
      static_cast<uint64_t>(tick_freq * min_time_between_events);
  }

  bool process_event()
  {
    ++event_count;
    uint64_t tick;
    QueryPerformanceCounter((LARGE_INTEGER*)&tick);
    if (event_count == 1 || event_count == event_limit ||
        tick - last_tick_count > min_tick_count_between_events) {
      last_tick_count = tick;
      return true;
    }
    return false;
  }
};

struct payload
{
  metadb_handle_ptr track;
  pfc::string8 name;
  bool in_library;
  std::shared_ptr<scan_handle> handle;
};

void
store_waveform_helper(std::shared_ptr<waveform_entry> wf, payload p)
{
  core_api::ensure_main_thread();
  unless_terminating([&] {
    auto true_data = encode_waveform_v1(*wf);
    std::vector<uint8_t> compress_buf(mz_compressBound(true_data.size()));
    mz_ulong bytes_compressed = compress_buf.size();
    if (MZ_OK == mz_compress2(compress_buf.data(),
                              &bytes_compressed,
                              true_data.data(),
                              true_data.size(),
                              9)) {
      sqlite3* db = p.in_library ? db_profile : db_memory;
      sqlite3_stmt* stmt = nullptr;
      sqlite3_prepare_v2(
        db, "REPLACE INTO waveform_v1 VALUES (?,?,?);", -1, &stmt, nullptr);
      sqlite3_bind_text(stmt, 1, p.name.c_str(), -1, SQLITE_STATIC);
      sqlite3_bind_int(stmt, 2, true_data.size());
      sqlite3_bind_blob(
        stmt, 3, compress_buf.data(), bytes_compressed, SQLITE_STATIC);
      sqlite3_step(stmt);
      sqlite3_finalize(stmt);
    }
  });
}

DWORD CALLBACK
foreground_analysis_work_item_helper(void* param)
{
  payload p = *(payload*)param;
  delete (payload*)param;
  try {
    waveform_scan_context ctx(p.track, p.handle->abort_cb);
    if (!ctx.song) {
      return 1;
    }
    rate_limit rl(ctx.slice_count, 0.01f);
    for (size_t i = 0; i < ctx.slice_count; ++i) {
      auto wf = scan_waveform(ctx);
      if (rl.process_event()) {
        g_waveform_ready(
          wf, p.track, static_cast<float>(i) / (ctx.slice_count + 1));
      }
    }
    auto wf = ctx.wf;
    fb2k::inMainThread(std::bind(&store_waveform_helper, wf, p));
  } catch (pfc::exception&) {
  }
  EnterCriticalSection(&scan_mutex);
  queued_action_scan = nullptr;
  LeaveCriticalSection(&scan_mutex);
  return 0;
}

void
background_analysis_helper(payload p, abort_callback& abort_cb)
{
  try {
    waveform_scan_context ctx(p.track, abort_cb);
    if (!ctx.song) {
      return;
    }
    for (size_t i = 0; i < ctx.slice_count; ++i) {
      auto wf = scan_waveform(ctx);
    }
    auto wf = ctx.wf;
    fb2k::inMainThread(std::bind(&store_waveform_helper, wf, p));
  } catch (pfc::exception&) {
  }
}

void
refresh_track(metadb_handle_ptr track)
{
  if (current_track == track) {
    return;
  }

  current_track = track;
  pfc::string8 new_name = format_track(track);
  current_name = new_name;

  auto wf = find_waveform(track);
  if (wf) {
    g_waveform_ready(wf, track, 1.0f);
    return;
  }

  EnterCriticalSection(&scan_mutex);

  if (queued_action_scan) {
    queued_action_scan->abort_cb.abort();
    queued_action_scan = nullptr;
  }

  auto sh = std::make_shared<scan_handle>();
  bool in_library = track_in_library_main_only(track);
  payload* user_data = new payload();
  user_data->track = track;
  user_data->name = new_name;
  user_data->in_library = in_library;
  user_data->handle = sh;
  queued_action_scan = sh;
  QueueUserWorkItem(&foreground_analysis_work_item_helper, user_data, 0);

  LeaveCriticalSection(&scan_mutex);
}

struct track_change_listener : play_callback_static
{

  //! Controls which methods your callback wants called; returned value should
  //! not change in run time, you should expect it to be queried only once (on
  //! startup). See play_callback::flag_* constants.
  unsigned get_flags() override
  {
    return play_callback::flag_on_playback_new_track;
  }
  //! Playback process is being initialized. on_playback_new_track() should be
  //! called soon after this when first file is successfully opened for
  //! decoding.
  void FB2KAPI on_playback_starting(play_control::t_track_command p_command,
                                    bool p_paused) override
  {}
  //! Playback advanced to new track.
  void FB2KAPI on_playback_new_track(metadb_handle_ptr p_track) override
  {
    refresh_track(p_track);
  }
  //! Playback stopped.
  void FB2KAPI on_playback_stop(play_control::t_stop_reason p_reason) override
  {
    current_track.release();
  }
  //! User has seeked to specific time.
  void FB2KAPI on_playback_seek(double p_time) override {}
  //! Called on pause/unpause.
  void FB2KAPI on_playback_pause(bool p_state) override {}
  //! Called when currently played file gets edited.
  void FB2KAPI on_playback_edited(metadb_handle_ptr p_track) override {}
  //! Dynamic info (VBR bitrate etc) change.
  void FB2KAPI on_playback_dynamic_info(file_info const& p_info) override {}
  //! Per-track dynamic info (stream track titles etc) change. Happens less
  //! often than on_playback_dynamic_info().
  void FB2KAPI on_playback_dynamic_info_track(file_info const& p_info) override
  {}
  //! Called every second, for time display
  void FB2KAPI on_playback_time(double p_time) override {}
  //! User changed volume settings. Possibly called when not playing.
  //! @param p_new_val new volume level in dB; 0 for full volume.
  void FB2KAPI on_volume_change(float p_new_val) override {}
};

static play_callback_static_factory_t<track_change_listener> g_tcl;

struct library_entry
{
  pfc::string8 track_id;
  metadb_handle_ptr handle;
};

struct background_analysis : threaded_process_callback
{
  std::vector<library_entry> queued_entries;

  void run(threaded_process_status& p_status, abort_callback& p_abort) override
  {
    auto sh = std::make_shared<scan_handle>();
    size_t n = queued_entries.size();
    for (size_t i = 0; i < n; ++i) {
      if (p_abort.is_aborting()) {
        break;
      }
      p_status.set_progress(i, n);
      auto&& e = queued_entries[i];
      payload p;
      p.track = e.handle;
      p.name = e.track_id;
      p.in_library = true;
      p.handle = sh;
      background_analysis_helper(p, p_abort);
    }
    p_status.set_progress(n, n);
  }
};

void
rescan_tracks(metadb_handle_list tracks)
{
  struct hot_potato
  {
    track_formatter tf;
    service_ptr_t<background_analysis> bg;
  };
  auto hp = std::make_shared<hot_potato>();
  hp->bg = new service_impl_t<background_analysis>();
  hp->bg->queued_entries.reserve(tracks.get_size());

  tracks.enumerate([hp](auto&& p) {
    library_entry le;
    le.handle = p;
    hp->bg->queued_entries.push_back(le);
  });

  std::thread([hp] {
    for (auto&& le : hp->bg->queued_entries) {
      le.track_id = hp->tf.format_track(le.handle);
    }

    fb2k::inMainThread([hp] {
      unsigned bg_flags = threaded_process::flag_show_abort |
                          threaded_process::flag_show_progress |
                          threaded_process::flag_show_delayed |
                          threaded_process::flag_no_focus;
      threaded_process::g_run_modeless(hp->bg,
                                       bg_flags,
                                       core_api::get_main_window(),
                                       "Regenerating minibar waveforms");
    });
  })
    .detach();
}

auto render_waveform_to_svg =
  [](std::shared_ptr<waveform_entry> wf) -> std::string {
  std::ostringstream oss;
  oss << R"(<svg version="1.1"
     baseProfile="full"
     xmlns="http://www.w3.org/2000/svg"
     xmlns:xlink="http://www.w3.org/1999/xlink"
     xmlns:ev="http://www.w3.org/2001/xml-events"
     viewBox="0 0 1024 200">
)";
  std::vector<float> coords;
  float right = 1024.0f;
  float bottom = 200.0f;
  if (wf) {
    int n = wf->sample_count;
    for (int col = 0; col < n; ++col) {
      float x = col * right / (n - 1);
      float max_val = -unmap_signed(wf->max_samples[col]);
      float y = bottom * (max_val / 2.0f + 0.5f);
      coords.push_back(x);
      coords.push_back(y);
    }
    for (int col = n - 1; col >= 0; --col) {
      float x = col * right / (n - 1);
      float min_val = -unmap_signed(wf->min_samples[col]);
      float y = bottom * (min_val / 2.0f + 0.5f);
      coords.push_back(x);
      coords.push_back(y);
    }
    // d="M 0 1 L 2 3 4 5 6 7 Z"
    // d="(0 1 )(L 2 3 )(4 5 )(6 7 )Z"
    oss << R"(<path class="waveform" d="M )";
    for (size_t i = 0; i < coords.size(); i += 2) {
      if (i == 1) {
        oss << "L ";
      }
      oss << coords[i] << "," << coords[i + 1] << " ";
    }
    oss << R"(Z"/>
)";
		oss << R"(<path class="limit_line" stroke="red" d="M 0,0 L 1024,0"/>)";
		oss << R"(<path class="zero_line" stroke="gray" d="M 0,100 L 1024,100"/>)";
		oss << R"(<path class="limit_line" stroke="red" d="M 0,200 L 1024,200"/>)";
  } else {
    // no waveform, placeholder
    oss << R"(<path class="invalid_line" stroke="darkred" d="M 0,0 L 1024,200"/>)";
    oss << R"(<path class="invalid_line" stroke="darkred" d="M 0,200 L 1024,0"/>)";
  }
  oss << R"("</svg>
)";
  return oss.str();
};

void
generate_report(metadb_handle_list_cref tracks)
{
  static std::string const header = R"(<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<style>.entry {
  background-color: aliceblue;
  border: 0.1em solid gray;
  border-radius: 1em;
  padding: 0.5em;
  margin: 0.2em;
}
.title {
  margin: 0.1em;
}
.filename {
  margin: 0.1em;
}
.limit_line {
  stroke: red;
}
.zero_line {
  stroke: black;
  stroke-opacity: 0.25;
}
.invalid_line {
  stroke: darkred;
}
.waveform {
  fill: #333;
}</style>
</head>
<body>
)";
  static std::string const footer = R"(</body>
</html>
)";

  DWORD cch_path = GetTempPathW(0, nullptr);
  std::wstring path(cch_path - 1, L'\0');
  GetTempPathW(cch_path, path.data());

  std::wstring filename = L"foo_wave_minibar-report.html";
  std::wstring fullpath = path + L"\\" + filename;

  HANDLE fh = CreateFileW(
    fullpath.c_str(), GENERIC_WRITE, 0, nullptr, CREATE_ALWAYS, 0, nullptr);
  if (fh == INVALID_HANDLE_VALUE) {
    return;
  }

  auto write_string = [](HANDLE fh, std::string str) {
    DWORD bytes_written{};
    WriteFile(fh, str.data(), str.size(), &bytes_written, nullptr);
  };

  DWORD bytes_written{};
  write_string(fh, header);

  for (size_t i = 0; i < tracks.get_size(); ++i) {
    write_string(fh, R"(<div class="entry">)");
    auto track = tracks[i];
    auto wf = find_waveform(track);
    auto svg = render_waveform_to_svg(wf);
    std::ostringstream oss;
    auto info = track->get_async_info_ref();
    auto artist = info->info().meta_get("artist", 0);
    auto title = info->info().meta_get("title", 0);
    if (artist && title) {
      oss << R"(<div class="title">)" << artist << " - " << title << "</div>\n";
    }
		else {
			auto& loc = track->get_location();
			oss << R"(<div class="filename">File: )" << loc.get_path() << ", subsong: "
				<< loc.get_subsong() << "</div>";
		}
    write_string(fh, oss.str());
    write_string(fh, svg);
    write_string(fh, "</div>");
  }

  write_string(fh, footer);

  CloseHandle(fh);

  ShellExecuteW(nullptr, L"open", fullpath.c_str(), nullptr, nullptr, SW_SHOW);
}

struct context_commands : contextmenu_item_simple
{
  unsigned get_num_items() override { return 2; }

  void get_item_name(unsigned p_index, pfc::string_base& p_out) override
  {
    std::array<char const*, 2> names = {
      "Regenerate minibar waveform", "Generate standalone waveform report"
    };
    p_out = names.at(p_index);
  }

  // executes in main thread
  void context_command(unsigned p_index,
                       metadb_handle_list_cref p_data,
                       GUID const& p_caller) override
  {
    if (p_index == 0) {
      rescan_tracks(p_data);
    } else if (p_index == 1) {
      generate_report(p_data);
    }
  }

  GUID get_item_guid(unsigned p_index) override
  {
    // {84275A52-0232-4FD6-AE05-851FD8D8075A}
    static GUID const guid_regenerate = {
      0x84275a52,
      0x232,
      0x4fd6,
      { 0xae, 0x5, 0x85, 0x1f, 0xd8, 0xd8, 0x7, 0x5a }
    };

    // {7D558EA7-5DEB-40C8-8D69-3AB27CFC74F5}
    static const GUID guid_generate_standalone_report = {
      0x7d558ea7,
      0x5deb,
      0x40c8,
      { 0x8d, 0x69, 0x3a, 0xb2, 0x7c, 0xfc, 0x74, 0xf5 }
    };

    std::array<GUID, 2> guids = { guid_regenerate,
                                  guid_generate_standalone_report };
    return guids.at(p_index);
  }

  bool get_item_description(unsigned p_index, pfc::string_base& p_out) override
  {
    std::array<char const*, 2> descs = {
      "Regenerate selected minibar waveforms in the background.",
      "Generate a HTML file with embedded waveform images."
    };
    p_out = descs.at(p_index);
    return true;
  }

  GUID get_parent() override { return contextmenu_groups::utilities; }
};

static contextmenu_item_factory_t<context_commands> g_context_commands;

struct utility_commands : mainmenu_commands
{
  t_uint32 get_command_count() override { return 1; }

  GUID get_command(t_uint32 p_index) override
  {
    // {18BDDE28-115D-44E9-A731-61D3780D667D}
    static GUID const guid_resync_with_library = {
      0x18bdde28,
      0x115d,
      0x44e9,
      { 0xa7, 0x31, 0x61, 0xd3, 0x78, 0xd, 0x66, 0x7d }
    };

    std::array<GUID, 1> guids = { guid_resync_with_library };
    return guids.at(p_index);
  }

  void get_name(t_uint32 p_index, pfc::string_base& p_out) override
  {
    std::array<char const*, 1> names = { "Sync minibar with library" };
    p_out = names.at(p_index);
  }

  bool get_description(t_uint32 p_index, pfc::string_base& p_out) override
  {
    std::array<char const*, 1> descs = {
      "Matches minibar database with media library, removing stale "
      "tracks and generating new tracks."
    };
    p_out = descs.at(p_index);
    return true;
  }

  GUID get_parent() override { return mainmenu_groups::library; }

  void execute_library_resync()
  {

    track_formatter tf;
    static_api_ptr_t<library_manager> lm;
    auto handles = std::make_unique<metadb_handle_list>();
    lm->get_all_items(*handles);
    console::printf("%d tracks in library", handles->get_size());
    std::thread([ tf, handles{ std::move(handles) } ] {
      tic_toc tt;

      auto entries = std::make_shared<std::vector<library_entry>>();
      entries->reserve(handles->get_size());
      handles->enumerate([&](auto&& h) {
        library_entry e = { tf.format_track(h), h };
        entries->emplace_back(std::move(e));
      });
      float time_taken = tt.measure();

      fb2k::inMainThread([entries, time_taken] {
        std::string table_name =
          std::string("\"") + pfc::format_guid_cpp(pfc::createGUID()).c_str() +
          "\"";
        std::string create_sql = "CREATE TEMP TABLE " + table_name +
                                 " (entry_serial INTEGER NOT NULL, "
                                 "track_id TEXT NOT NULL);";
        std::string insert_sql = "INSERT INTO " + table_name + " VALUES (?,?);";
        std::string delete_sql = "DELETE FROM waveform_v1 WHERE "
                                 "track_id NOT IN (SELECT track_id "
                                 "FROM " +
                                 table_name + ");";
        std::string missing_sql =
          "SELECT entry_serial FROM " + table_name + " WHERE " +
          "track_id NOT IN (SELECT track_id FROM waveform_v1);";
        std::string drop_sql = "DROP TABLE " + table_name + ";";

        sqlite3_exec(db_profile, create_sql.c_str(), nullptr, nullptr, nullptr);

        sqlite3_exec(db_profile, "BEGIN", nullptr, nullptr, nullptr);

        sqlite3_stmt* stmt = nullptr;
        sqlite3_prepare_v2(
          db_profile, insert_sql.c_str(), insert_sql.size(), &stmt, nullptr);

        for (size_t i = 0; i < entries->size(); ++i) {
          auto&& e = (*entries)[i];
          sqlite3_bind_int(stmt, 1, i);
          sqlite3_bind_text(stmt,
                            2,
                            e.track_id.c_str(),
                            e.track_id.get_length(),
                            SQLITE_STATIC);
          sqlite3_step(stmt);
          sqlite3_reset(stmt);
        }
        sqlite3_exec(db_profile, "COMMIT", nullptr, nullptr, nullptr);
        sqlite3_finalize(stmt);

        sqlite3_exec(db_profile, delete_sql.c_str(), nullptr, nullptr, nullptr);

        sqlite3_prepare_v2(
          db_profile, missing_sql.c_str(), missing_sql.size(), &stmt, nullptr);

        service_ptr_t<background_analysis> bg =
          new service_impl_t<background_analysis>();

        while (SQLITE_ROW == sqlite3_step(stmt)) {
          size_t missing_id = (size_t)sqlite3_column_int(stmt, 0);
          bg->queued_entries.push_back((*entries)[missing_id]);
        }
        console::printf("%d tracks not in database yet",
                        bg->queued_entries.size());
        sqlite3_finalize(stmt);

        sqlite3_exec(db_profile, drop_sql.c_str(), nullptr, nullptr, nullptr);

        if (!bg->queued_entries.empty()) {
          unsigned bg_flags = threaded_process::flag_show_abort |
                              threaded_process::flag_show_progress |
                              threaded_process::flag_show_delayed |
                              threaded_process::flag_no_focus;
          threaded_process::g_run_modeless(
            bg,
            bg_flags,
            core_api::get_main_window(),
            "Synchronizing minibar with media library");
        }
      });
    })
      .detach();
  }

  void execute_standalone_report_generation() {}

  // executes in main thread
  void execute(t_uint32 p_index,
               service_ptr_t<service_base> p_callback) override
  {
    if (p_index = 0) {
      execute_library_resync();
    } else if (p_index == 1) {
      execute_standalone_report_generation();
    }
  }
};

static mainmenu_commands_factory_t<utility_commands> g_ucs;

t_ui_color
mix_colors(t_ui_color a, t_ui_color b, float amount)
{
  float inv = 1.0f - amount;
  return RGB(GetRValue(a) * inv + GetRValue(b) * amount,
             GetGValue(a) * inv + GetGValue(b) * amount,
             GetBValue(a) * inv + GetBValue(b) * amount);
}

// Unlike CMemoryDC, this doesn't shift the origin, nor does it blit on
// destruction
class memory_dc_no_blit : public CDC
{
public:
  // Data members
  HDC m_hDCOriginal;
  CBitmap m_bmp;
  HBITMAP m_hBmpOld;

  // Constructor/destructor
  memory_dc_no_blit(HDC hDC, CSize const& size)
    : m_hDCOriginal(hDC)
    , m_hBmpOld(NULL)
  {
    CreateCompatibleDC(m_hDCOriginal);
    ATLASSERT(m_hDC != NULL);
    m_bmp.CreateCompatibleBitmap(m_hDCOriginal, size.cx, size.cy);
    ATLASSERT(m_bmp.m_hBitmap != NULL);
    m_hBmpOld = (SelectBitmap)(m_bmp);
  }

  ~memory_dc_no_blit() { (SelectBitmap)(m_hBmpOld); }
};

// parses "#RRGGBB", "RRGBBB", "0.1 0.2 0.3", and "128 35 255"
t_ui_color
parse_config_color(pfc::string8 str)
{
  std::istringstream iss(str.c_str());
  std::vector<std::string> parts;
  std::string part;
  while (std::getline(iss, part, ' ')) {
    parts.push_back(part);
  }
  int r = 0, g = 0, b = 0;
  if (parts.size() == 1) {
    auto part = parts[0];
    if (part.size() == 7 && part[0] == '#') {
      r = static_cast<int>(strtol(part.substr(1, 2).c_str(), nullptr, 16));
      g = static_cast<int>(strtol(part.substr(3, 2).c_str(), nullptr, 16));
      b = static_cast<int>(strtol(part.substr(5, 2).c_str(), nullptr, 16));
    } else if (part.size() == 6) {
      r = static_cast<int>(strtol(part.substr(0, 2).c_str(), nullptr, 16));
      g = static_cast<int>(strtol(part.substr(2, 2).c_str(), nullptr, 16));
      b = static_cast<int>(strtol(part.substr(4, 2).c_str(), nullptr, 16));
    }
  } else if (parts.size() == 3) {
    int* rgb[] = { &r, &g, &b };
    for (size_t i = 0; i < 3; ++i) {
      auto part = parts[i];
      if (part.find('.') != std::string::npos) {
        *rgb[i] = (int)(std::stof(part, nullptr) * 255.0f);
      } else {
        *rgb[i] = std::stoi(part, nullptr, 10);
      }
    }
  }
  r = (std::max)(0, (std::min)(255, r));
  g = (std::max)(0, (std::min)(255, g));
  b = (std::max)(0, (std::min)(255, b));
  return RGB(r, g, b);
}

static t_ui_color
parse_config_color(advconfig_string_factory& variable)
{
  pfc::string8 str;
  variable.get(str);
  return parse_config_color(str);
}

static rgb
bgr_to_rgb(COLORREF c)
{
  return { GetBValue(c), GetGValue(c), GetRValue(c) };
}

struct bar_window
  : CWindowImpl<bar_window>
  , waveform_callback
{
  DECLARE_WND_CLASS_EX(TEXT("{C5AD4C33-9458-4747-BAB6-3D7CC971B842}"),
                       CS_VREDRAW | CS_HREDRAW,
                       (-1));

  virtual bool forward_rmb() const { return false; }

  virtual bool refresh_colors()
  {
    bool has_changes = false;
    auto played_bg_color = parse_config_color(g_played_background);
    auto played_fg_color = parse_config_color(g_played_fill);
    auto played_rms_color = parse_config_color(g_played_rms);
    auto unplayed_bg_color = parse_config_color(g_unplayed_background);
    auto unplayed_fg_color = parse_config_color(g_unplayed_fill);
    auto unplayed_rms_color = parse_config_color(g_unplayed_rms);

    if (played_colors.bg_color != played_bg_color) {
      has_changes = true;
      if (played_background_brush) {
        played_background_brush.DeleteObject();
      }
      played_background_brush.CreateSolidBrush(played_bg_color);
      played_colors.bg_color = played_bg_color;
    }
    if (played_colors.fg_color != played_fg_color) {
      has_changes = true;
      played_colors.fg_color = played_fg_color;
    }
    if (played_colors.rms_color != played_rms_color) {
      has_changes = true;
      played_colors.rms_color = played_rms_color;
    }

    if (unplayed_colors.bg_color != unplayed_bg_color) {
      has_changes = true;
      if (unplayed_background_brush) {
        unplayed_background_brush.DeleteObject();
      }
      unplayed_background_brush.CreateSolidBrush(unplayed_bg_color);
      unplayed_colors.bg_color = unplayed_bg_color;
    }
    if (unplayed_colors.fg_color != unplayed_fg_color) {
      has_changes = true;
      unplayed_colors.fg_color = unplayed_fg_color;
    }
    if (unplayed_colors.rms_color != unplayed_rms_color) {
      has_changes = true;
      unplayed_colors.rms_color = unplayed_rms_color;
    }
    return has_changes;
  }

  void on_current_waveform(std::shared_ptr<waveform_entry> wf,
                           metadb_handle_ptr track,
                           float percentage) override
  {
    if (this->current_name != ::current_name) {
      current_percentage = -1.0f;
      this->current_name = ::current_name;
    }
    if (current_track.is_valid() &&
        current_track->get_location() == track->get_location() &&
        current_percentage < percentage) {
      current_waveform = wf;
      current_percentage = percentage;
      bitmaps_valid = false;
      Invalidate();
    }
  }

  BEGIN_MSG_MAP(bar_window)
  MSG_WM_CREATE(on_wm_create)
  MSG_WM_DESTROY(on_wm_destroy)
  MSG_WM_LBUTTONDOWN(om_wm_lbuttondown)
  MSG_WM_LBUTTONUP(om_wm_lbuttonup)
  MSG_WM_RBUTTONUP(on_wm_rbuttonup)
  MSG_WM_MOUSEMOVE(on_wm_mousemove)
  MSG_WM_PAINT(on_wm_paint)
  MSG_WM_SIZE(on_wm_size)
  MSG_WM_TIMER(on_wm_timer)
  END_MSG_MAP()

  enum drag_state
  {
    k_not_dragging,
    k_dragging,
    k_suspended
  };
  drag_state drag;
  CPoint move_target;

  int on_wm_create(CREATESTRUCT* cs)
  {
    drag = k_not_dragging;
    current_percentage = -1.0f;
    bitmaps_valid = false;
    refresh_colors();
    cursor_boost_brush.CreateSolidBrush(RGB(200, 200, 200));
    static_api_ptr_t<waveform_callback_manager>()->register_callback(this);
    SetTimer(refresh_timer_id, 1000 / 60, nullptr);
    {
      static_api_ptr_t<playback_control> pc;
      metadb_handle_ptr current;
      if (pc->get_now_playing(current)) {
        refresh_track(current);
      } else {
        static_api_ptr_t<playlist_manager> pm;
        if (pm->activeplaylist_get_focus_item_handle(current)) {
          refresh_track(current);
        }
      }
    }
    return TRUE;
  }

  void on_wm_destroy()
  {
    static_api_ptr_t<waveform_callback_manager>()->unregister_callback(this);
    KillTimer(refresh_timer_id);
  }

  std::unique_ptr<seek_tooltip> seek_tip;

  void om_wm_lbuttondown(UINT flags, CPoint point)
  {
    SetCapture();
    static_api_ptr_t<playback_control> pbc;
    if (pbc->is_playing() || pbc->is_paused()) {
      drag = k_dragging;
      if (!seek_tip) {
        seek_tip = std::make_unique<seek_tooltip>(*this);
      }
      seek_tip->on_seek_begin();
    }
    move_target = point;
  }

  void om_wm_lbuttonup(UINT flags, CPoint point)
  {
    ReleaseCapture();
    if (drag == k_dragging) {
      seek_tip->on_seek_end(false);
      CRect rc;
      GetClientRect(rc);
      move_target = point;
      double new_time = rc.Width() ? (move_target.x / (double)rc.Width()) : 0.0;
      static_api_ptr_t<playback_control> pbc;
      if (pbc->is_playing() || pbc->is_paused()) {
        pbc->playback_seek(new_time * pbc->playback_get_length());
      }
    }
    drag = k_not_dragging;
  }

  void on_wm_rbuttonup(UINT wparam, CPoint point)
  {
    if (forward_rmb()) {
      SetMsgHandled(FALSE);
    }
  }

  void on_wm_mousemove(UINT flags, CPoint point)
  {
    if (drag == k_not_dragging) {
      return;
    }
    if (move_target.x == point.x) {
      return;
    }
    move_target = point;
    CRect rc;
    GetClientRect(rc);
    double target_time =
      rc.Width() ? (move_target.x / (double)rc.Width()) : 0.0;
    enum
    {
      border = 50
    };
    rc.InflateRect(border, border);
    if (!rc.PtInRect(point)) {
      drag = k_suspended;
    } else {
      drag = k_dragging;
    }
    static_api_ptr_t<playback_control> pbc;
    if (pbc->is_playing() || pbc->is_paused()) {
      seek_tip->on_seek_position(target_time * pbc->playback_get_length(),
                                 drag == k_dragging);
    }
  }

  void on_wm_paint(CDCHandle)
  {
    CPaintDC dc(*this);
    CRect rc;
    GetClientRect(&rc);
    if (dc) {
      if (refresh_colors()) {
        bitmaps_valid = false;
      }
      auto wf = current_waveform;
      if (wf) {
        if (!bitmaps_valid) {
          tic_toc tt;
          long width = rc.right;
          long height = rc.bottom;
          long byte_width = width * 3;
          long line_stride =
            byte_width + ((byte_width % 4) ? (4 - (byte_width % 4)) : 0);

          if (!dcs || dcs->width != width || dcs->height != height) {
            dcs = std::make_shared<dc_pair>(CDCHandle(dc), rc);
          }

          BITMAPINFO bmi = {};
          auto& _ = bmi.bmiHeader;
          _.biSize = sizeof(BITMAPINFOHEADER);
          _.biWidth = width;
          _.biHeight = -height;
          _.biPlanes = 1;
          _.biBitCount = 24;
          _.biCompression = BI_RGB;

          color_set* colors[] = {
            &unplayed_colors,
            &played_colors,
          };

          memory_dc_no_blit* targets[] = {
            &dcs->unplayed_image,
            &dcs->played_image,
          };

          image_based_sampling_algorithm sampler(wf.get(), width);
          uint8_t* pixels = (uint8_t*)calloc(height, line_stride);
          for (size_t target = 0; target < _countof(targets); ++target) {
            auto& cs = *colors[target];
            auto& target_dc = *targets[target];

            rgb bg_rgb = bgr_to_rgb(cs.bg_color);
            rgb rms_rgb = bgr_to_rgb(cs.rms_color);
            rgb fg_rgb = bgr_to_rgb(cs.fg_color);

            rgb fills[] = { bg_rgb, fg_rgb, rms_rgb, fg_rgb, bg_rgb };

            render_waveform_to_buffer(sampler,
                                      pixels,
                                      width,
                                      height,
                                      line_stride,
                                      fills,
                                      _countof(fills));
            target_dc.GetCurrentBitmap().SetDIBits(
              target_dc, 0, height, pixels, &bmi, DIB_RGB_COLORS);
          }
          free(pixels);
          bitmaps_valid = true;
#if 0
          console::printf("Regen time: %s ms",
                          std::to_string(1000.0f * tt.measure()).c_str());
#endif
        }
        long width = rc.right;
        long height = rc.bottom;
        long byte_width = width * 3;
        long line_stride =
          byte_width + ((byte_width % 4) ? (4 - (byte_width % 4)) : 0);

        static_api_ptr_t<play_control> pc;
        float progress = 0.0f;
        if (drag == k_dragging) {
          progress = move_target.x / (float)width;
        } else {
          double play_pos = pc->playback_get_position();
          double play_len = pc->playback_get_length();
          progress = play_len ? static_cast<float>(play_pos / play_len) : 0.0f;
        }
        long play_col = (long)(progress * width);
        float play_frac = fmodf(progress * width, 1.0f);

        memory_dc_no_blit comp_dc(dc, CSize(1, height));
        BLENDFUNCTION bf = { AC_SRC_OVER, 0, (uint8_t)(play_frac * 255.0f), 0 };
        comp_dc.BitBlt(
          0, 0, 1, height, dcs->unplayed_image, play_col, 0, SRCCOPY);
        comp_dc.AlphaBlend(
          0, 0, 1, height, dcs->played_image, play_col, 0, 1, height, bf);

        int dy = static_cast<int>(height / 2 * 0.1f);
#ifdef MINIBAR_SHIMMERING_CURSOR
        memory_dc_no_blit cursor_dc(dc, CSize(1, height));
        cursor_dc.FillRect(CRect(0, 0, 1, height), cursor_boost_brush);
        {
          BLENDFUNCTION bf = { AC_SRC_OVER, 0, 0x20, 0 };
          comp_dc.AlphaBlend(0, 0, 1, height, cursor_dc, 0, 0, 1, height, bf);
        }
#endif

        dc.BitBlt(0, 0, play_col, height, dcs->played_image, 0, 0, SRCCOPY);
        dc.BitBlt(play_col, 0, 1, height, comp_dc, 0, 0, SRCCOPY);
        dc.BitBlt(play_col + 1,
                  0,
                  width - play_col - 1,
                  height,
                  dcs->unplayed_image,
                  play_col + 1,
                  0,
                  SRCCOPY);
      } else {
        dc.FillRect(rc, unplayed_background_brush);
      }
    }
    ValidateRect(nullptr);
  }

  void on_wm_size(UINT type, CSize size) { bitmaps_valid = false; }

  void on_wm_timer(UINT_PTR id)
  {
    if (id == refresh_timer_id) {
      InvalidateRect(nullptr);
    }
  }

  enum
  {
    refresh_timer_id = 0x4242,
  };

  std::shared_ptr<waveform_entry> current_waveform;
  pfc::string8 current_name;
  float current_percentage;

  bool bitmaps_valid;
  struct dc_pair
  {
    explicit dc_pair(CDCHandle dc, CRect rc)
      : played_image(dc, rc.Size())
      , unplayed_image(dc, rc.Size())
      , width(rc.Width())
      , height(rc.Height())
    {}

    memory_dc_no_blit played_image, unplayed_image;
    long width, height;
  };
  std::shared_ptr<dc_pair> dcs;
  CBrush played_background_brush;
  CBrush unplayed_background_brush;
  CBrush cursor_boost_brush;

  struct color_set
  {
    t_ui_color bg_color, fg_color, rms_color;
  };
  color_set played_colors, unplayed_colors;
};

struct minibar_dui
  : ui_element_instance
  , bar_window
{
  static GUID g_get_guid()
  {
    // {8DDF28FE-440F-4B89-9DCD-A7980A229EB0}
    static GUID const guid = {
      0x8ddf28fe,
      0x440f,
      0x4b89,
      { 0x9d, 0xcd, 0xa7, 0x98, 0xa, 0x22, 0x9e, 0xb0 }
    };
    return guid;
  }

  static GUID g_get_subclass()
  {
    return ui_element_subclass_playback_information;
  }

  static void g_get_name(pfc::string_base& out) { out = "Waveform Minibar"; }

  static ui_element_config::ptr g_get_default_configuration()
  {
    return ui_element_config::g_create_empty(g_get_guid());
  }

  static char const* g_get_description()
  {
    return "A minimal variant of a waveform seekbar.";
  }

  void set_configuration(ui_element_config::ptr cfg) { cfg = cfg; }

  ui_element_config::ptr get_configuration() { return cfg; }

  minibar_dui(ui_element_config::ptr cfg,
              ui_element_instance_callback::ptr callback)
    : cfg(cfg)
    , callback(callback)
  {}

  void initialize_window(HWND parent)
  {
    DWORD extended_style = (g_show_window_edge.get() ? WS_EX_STATICEDGE : 0);
    Create(parent, nullptr, nullptr, WS_CHILD, extended_style);
  }

  void notify(GUID const& what, size_t p1, void const* p2, size_t p2_size)
  {
    if (what == ui_element_notify_colors_changed) {
      refresh_colors();
      Invalidate();
    }
  }

  bool forward_rmb() const override { return callback->is_edit_mode_enabled(); }

  ui_element_config::ptr cfg;
  ui_element_instance_callback::ptr callback;
};

namespace uie {
template<typename W, typename T = window>
struct container_atl_window
  : W
  , T
{
  window_host_ptr host;

  HWND create_or_transfer_window(HWND parent,
                                 window_host_ptr const& new_host,
                                 ui_helpers::window_position_t const& position)
  {
    if ((HWND) * this) {
      ShowWindow(SW_HIDE);
      SetParent(parent);
      host->relinquish_ownership(*this);
      host = new_host;

      SetWindowPos(
        0, position.x, position.y, position.cx, position.cy, SWP_NOZORDER);
    } else {
      host = new_host;
      CRect r;
      position.convert_to_rect(r);
      DWORD extended_style = (g_show_window_edge.get() ? WS_EX_STATICEDGE : 0);
      Create(parent, r, 0, WS_CHILD, extended_style);
    }

    return *this;
  }

  virtual void destroy_window()
  {
    ::DestroyWindow(*this);
    host.release();
  }

  virtual bool is_available(window_host_ptr const& p) const { return true; }

  window_host_ptr const& get_host() const { return host; }

  virtual HWND get_wnd() const { return *this; }
};
}

struct minibar_uie_base : uie::container_atl_window<bar_window>
{
  virtual void set_config(stream_reader* p_reader,
                          t_size p_size,
                          abort_callback& p_abort);
  virtual void get_config(stream_writer* p_writer,
                          abort_callback& p_abort) const;

  virtual void get_name(pfc::string_base& out) const;
};

extern GUID const s_panel_guid, s_toolbar_guid;

template<uie::window_type_t WindowType>
struct minibar_uie_t : minibar_uie_base
{
  virtual void get_category(pfc::string_base& out) const
  {
    switch (WindowType) {
      case uie::type_toolbar:
        out.set_string("Toolbars");
        return;
      case uie::type_panel:
      default:
        out.set_string("Panels");
        return;
    }
  }

  virtual GUID const& get_extension_guid() const
  {
    switch (WindowType) {
      case uie::type_toolbar:
        return s_toolbar_guid;
      case uie::type_panel:
      default:
        return s_panel_guid;
    }
  }

  virtual unsigned get_type() const { return WindowType; }
};

void
minibar_uie_base::get_name(pfc::string_base& out) const
{
  out.set_string("Waveform minibar");
}

// {86BE9A75-CBA4-45D2-8F71-0A378080FBFE}
GUID const s_panel_guid = { 0x86be9a75,
                            0xcba4,
                            0x45d2,
                            { 0x8f, 0x71, 0xa, 0x37, 0x80, 0x80, 0xfb, 0xfe } };

// {AF426535-3CAC-42FA-B03C-4AB4A6B3F8C2}
GUID const s_toolbar_guid = {
  0xaf426535,
  0x3cac,
  0x42fa,
  { 0xb0, 0x3c, 0x4a, 0xb4, 0xa6, 0xb3, 0xf8, 0xc2 }
};

void
minibar_uie_base::set_config(stream_reader* p_reader,
                             t_size p_size,
                             abort_callback& p_abort)
{}
void
minibar_uie_base::get_config(stream_writer* p_writer,
                             abort_callback& p_abort) const
{}

static service_factory_t<ui_element_impl<minibar_dui>> g_bar_window;

static uie::window_factory<minibar_uie_t<uie::type_panel>> g_sadf_panel;
static uie::window_factory<minibar_uie_t<uie::type_toolbar>> g_sadf_toolbar;

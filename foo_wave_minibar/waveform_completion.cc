﻿#include "waveform_completion.h"

#include <mutex>
#include <set>
#include <vector>

namespace {
std::set<waveform_callback*> cbs;
struct waveform_callback_manager_impl : waveform_callback_manager
{
  waveform_callback_manager_impl() { console::print("hi!"); }
  void register_callback(waveform_callback* callback) override
  {
    core_api::ensure_main_thread();
    cbs.insert(callback);
  }

  void unregister_callback(waveform_callback* callback) override
  {
    core_api::ensure_main_thread();
    cbs.erase(callback);
  }

  void dispatch_current_waveform(std::shared_ptr<waveform_entry> wf,
                                 metadb_handle_ptr track,
                                 float percentage) override
  {
    fb2k::inMainThread([=] {
      for (auto&& cb : cbs) {
        cb->on_current_waveform(wf, track, percentage);
      }
    });
  }

  void dispatch_background_waveform(std::shared_ptr<waveform_entry> wf,
                                    metadb_handle_ptr track) override
  {
    fb2k::inMainThread([=] {
      for (auto&& cb : cbs) {
        cb->on_background_waveform(wf, track);
      }
    });
  }
};

service_factory_single_t<waveform_callback_manager_impl> g_wcmi;
}
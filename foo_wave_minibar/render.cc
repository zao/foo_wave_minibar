﻿#include "render.h"

#include "analysis.h"

#include <algorithm>

#include "stb_image_resize.h"

image_based_sampling_algorithm::image_based_sampling_algorithm(
  waveform_entry const* wf,
  size_t target_width)
  : wf(wf)
  , target_width(target_width)
  , min_target(new float[target_width])
  , max_target(new float[target_width])
  , rms_target(new float[target_width])
{
  auto input_width = wf->sample_count;
  std::unique_ptr<float[]> input = std::make_unique<float[]>(input_width * 3);
  std::unique_ptr<float[]> output = std::make_unique<float[]>(target_width * 3);
  {
    float* p = input.get();
    for (size_t i = 0; i < input_width; ++i) {
      *p++ = unmap_signed(wf->min_samples[i]);
    }
    for (size_t i = 0; i < input_width; ++i) {
      *p++ = unmap_signed(wf->max_samples[i]);
    }
    for (size_t i = 0; i < input_width; ++i) {
      *p++ = unmap_unsigned(wf->rms_samples[i]);
    }
  }

  stbir_filter filter = STBIR_FILTER_MITCHELL;
  stbir_resize_float_generic(input.get(),
                             input_width,
                             3,
                             input_width * sizeof(float),
                             output.get(),
                             target_width,
                             3,
                             target_width * sizeof(float),
                             1,
                             STBIR_ALPHA_CHANNEL_NONE,
                             0,
                             STBIR_EDGE_CLAMP,
                             filter,
                             STBIR_COLORSPACE_LINEAR,
                             nullptr);

  memcpy(min_target.get(), output.get(), target_width * sizeof(float));
  memcpy(max_target.get(),
         output.get() + target_width,
         target_width * sizeof(float));
  memcpy(rms_target.get(),
         output.get() + 2 * target_width,
         target_width * sizeof(float));
}

sample_value
image_based_sampling_algorithm::sample_at(long col) const
{
  assert(col >= 0);
  assert(col < target_width);
  sample_value val = { min_target[col], max_target[col], rms_target[col] };
  return val;
}

long
image_based_sampling_algorithm::column_count() const
{
  return wf->sample_count;
}

void
render_waveform_to_buffer(image_based_sampling_algorithm const& sampler,
                          uint8_t* pixels,
                          long width,
                          long height,
                          long line_stride,
                          rgb* fills,
                          size_t num_fills)
{
  for (long target_col = 0; target_col < width; ++target_col) {
    long mid = height / 2;
    sample_value val = sampler.sample_at(target_col);
    long upper_edge = (long)(mid - height / 2 * val.max_value);
    long upper_rms = (long)(mid - height / 2 * +val.rms_value);
    long lower_rms = (long)(mid - height / 2 * -val.rms_value);
    long lower_edge = (long)(mid - height / 2 * val.min_value);
    long edges[] = { 0, upper_edge, upper_rms, lower_rms, lower_edge, height };
    int edge_count = _countof(edges);
    for (auto& e : edges) {
      e = (std::max)(0l, (std::min)(height, e));
    }
    for (size_t i = 0; i < num_fills; ++i) {
      long from = edges[i], to = edges[i + 1];
      rgb fill = fills[i];
      for (long row = from; row < to; ++row) {
        rgb* line_start = (rgb*)(pixels + row * line_stride);
        rgb* pixel = &line_start[target_col];
        *pixel = fill;
      }
    }
  }
}
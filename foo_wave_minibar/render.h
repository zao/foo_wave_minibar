﻿#pragma once

#include <stdint.h>

#include <memory>

struct waveform_entry;

struct sample_value
{
  float min_value, max_value, rms_value;
};

struct sampling_algorithm
{
  virtual sample_value sample_at(long col) const = 0;
  virtual long column_count() const = 0;
};

struct image_based_sampling_algorithm : sampling_algorithm
{
  image_based_sampling_algorithm(waveform_entry const* wf, size_t target_width);

  sample_value sample_at(long col) const override;

  long column_count() const override;

private:
  waveform_entry const* const wf;
  long const target_width;
  std::unique_ptr<float[]> min_target, max_target, rms_target;
};

struct rgb
{
  uint8_t b, g, r;
};

void
render_waveform_to_buffer(image_based_sampling_algorithm const& sampler,
                          uint8_t* pixels,
                          long width,
                          long height,
                          long line_stride,
                          rgb* fills,
                          size_t num_fills);
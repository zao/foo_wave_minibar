﻿#include "analysis.h"

struct song_slicer
{
  song_slicer(size_t slice_count,
              playable_location const& loc,
              abort_callback& abort_cb)
    : abort_cb(abort_cb)
    , chunk_offset(0)
    , track_sample_count(0)
    , current_slice(0)
    , slice_count(slice_count)
    , current_min(0.0f)
    , current_max(0.0f)
    , current_rms(0.0f)
  {
    file_info_impl fi;
    try {
      input_entry::g_open_for_decoding(
        decoder, nullptr, loc.get_path(), abort_cb);
      if (decoder.is_valid()) {
        decoder->initialize(
          loc.get_subsong(), input_flag_simpledecode, abort_cb);
        decoder->get_info(loc.get_subsong(), fi, abort_cb);
        track_sample_count = fi.info_get_length_samples();
        if (!decoder->run(current_chunk, abort_cb)) {
          current_chunk.set_channels(1);
          current_chunk.set_silence(1 << 10);
        }
        double td = 0.0;
        decoder->get_dynamic_info(fi, td);
        if (auto dynamic_track_sample_count = fi.info_get_length_samples()) {
          track_sample_count = dynamic_track_sample_count;
        }
        current_chunk = downmix_chunk(current_chunk);
      }
    } catch (pfc::exception&) {
      decoder.release();
    }
  }

  void analyze_slice()
  {
    if (decoder.is_empty()) {
      ++current_slice;
      return;
    }

    current_min = +FLT_MAX;
    current_max = -FLT_MAX;
    current_rms = 0.0f;
    uint64_t start_sample = track_sample_count * current_slice / slice_count;
    uint64_t end_sample =
      track_sample_count * (current_slice + 1) / slice_count;
    uint64_t samples_left_in_slice = end_sample - start_sample;

    while (samples_left_in_slice) {
      uint64_t usable_chunk_samples =
        pfc::min_t(samples_left_in_slice,
                   (uint64_t)current_chunk.get_sample_count() - chunk_offset);
      if (0 == usable_chunk_samples) {
        if (!decoder->run(current_chunk, abort_cb)) {
          current_chunk.set_silence(1 << 10);
        } else {
          current_chunk = downmix_chunk(current_chunk);
        }
        chunk_offset = 0;
        continue;
      }
      audio_sample const* samples = current_chunk.get_data() + chunk_offset;
      for (uint64_t i = 0; i < usable_chunk_samples; ++i) {
        audio_sample sample = samples[i];
        current_min = pfc::min_t(current_min, sample);
        current_max = pfc::max_t(current_max, sample);
        current_rms += sample * sample;
      }
      samples_left_in_slice -= usable_chunk_samples;
      chunk_offset += usable_chunk_samples;
    }

    current_rms =
      sqrtf(current_rms / static_cast<float>(end_sample - start_sample));
    ++current_slice;
  }

  bool valid() const { return decoder.is_valid(); }
  bool done() const { return current_slice == slice_count; }

  abort_callback& abort_cb;
  service_ptr_t<input_decoder> decoder;
  audio_chunk_impl current_chunk;
  uint64_t chunk_offset;

  uint64_t track_sample_count;

  size_t current_slice;
  size_t slice_count;

  audio_sample current_min;
  audio_sample current_max;
  audio_sample current_rms;
};

waveform_scan_context::waveform_scan_context(metadb_handle_ptr track,
                                             abort_callback& abort_cb)
  : slice_index(0)
  , slice_count(4096)
  , track(track)
  , abort_cb(abort_cb)
{
  song = std::make_shared<song_slicer>(slice_count, track->get_location(), abort_cb);
  if (!song->valid()) {
    song.reset();
  }
  wf = std::make_shared<waveform_entry>(static_cast<uint16_t>(slice_count));
}

waveform_scan_context::~waveform_scan_context()
{
  if (song && song->valid()) {
    char buf[64] = {};
    sprintf(buf, "%.3f", 1000.0f * tt.measure());
    console::printf("Analysis time: %s ms", buf);
  }
}

std::shared_ptr<waveform_entry>
scan_waveform(waveform_scan_context& ctx)
{
  auto& song = ctx.song;
  if (!song->done()) {
    song->analyze_slice();
    ctx.wf->min_samples[ctx.slice_index] = map_signed(song->current_min);
    ctx.wf->max_samples[ctx.slice_index] = map_signed(song->current_max);
    ctx.wf->rms_samples[ctx.slice_index] = map_unsigned(song->current_rms);
    ++ctx.slice_index;
  }
  return ctx.wf;
}

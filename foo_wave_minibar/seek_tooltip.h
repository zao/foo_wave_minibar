#pragma once

#include <ATLHelpers/ATLHelpers.h>

class seek_tooltip
{
public:
  explicit seek_tooltip(HWND parent)
    : parent(parent)
    , show(false)
  {
    tooltip.Create(nullptr);
    toolinfo.cbSize = sizeof(toolinfo);
    toolinfo.uFlags = TTF_TRACK | TTF_IDISHWND | TTF_ABSOLUTE | TTF_TRANSPARENT;
    toolinfo.hwnd = parent;
    toolinfo.uId = (UINT_PTR)(HWND)tooltip;
    toolinfo.lpszText = L"";
    toolinfo.hinst = core_api::get_my_instance();
    tooltip.AddTool(&toolinfo);
    old_wndproc = SubclassWindow(tooltip, &subclass_wndproc);
    SetWindowLongPtr(tooltip, GWLP_USERDATA, (LONG_PTR)this);
  }

  ~seek_tooltip()
  {
    SubclassWindow(tooltip, old_wndproc);
    tooltip.DelTool(&toolinfo);
    tooltip.DestroyWindow();
  }

  void on_seek_begin()
  {
    show = true;
    track_mouse();
  }

  void on_seek_position(double time, bool legal)
  {
    show = legal;
    std::wstring txt = format_time(time);
    toolinfo.lpszText = const_cast<wchar_t*>(txt.c_str());
    tooltip.SetToolInfo(&toolinfo);
    track_mouse();
  }

  void on_seek_end(bool aborted)
  {
    show = false;
    tooltip.TrackActivate(&toolinfo, FALSE);
  }

private:
  seek_tooltip(seek_tooltip const&) = delete;
  seek_tooltip& operator=(seek_tooltip const&) = delete;

  WNDPROC old_wndproc;

  static LRESULT subclass_wndproc(HWND wnd,
                                  UINT msg,
                                  WPARAM wparam,
                                  LPARAM lparam)
  {
    auto* self = (seek_tooltip*)GetWindowLongPtr(wnd, GWLP_USERDATA);
    WNDPROC wnd_proc = self->old_wndproc;
    if (msg == WM_NCHITTEST) {
      // common controls v6 return bogus values for hit testing, thus this
      // workaround
      return HTTRANSPARENT;
    }
    return CallWindowProc(wnd_proc, wnd, msg, wparam, lparam);
  }

  void track_mouse()
  {
    POINT pos = {};
    GetCursorPos(&pos);
    tooltip.TrackPosition(pos.x + 10, pos.y - 20);
    tooltip.TrackActivate(&toolinfo, show ? TRUE : FALSE);
  }

  std::wstring format_time(double time)
  {
    auto str =
      pfc::stringcvt::string_os_from_utf8(pfc::format_time(pfc::rint64(time)));
    std::wstring out = str.get_ptr();
    return out;
  }

  CToolTipCtrl tooltip;
  TOOLINFO toolinfo;
  CWindow parent;
  bool show;
};

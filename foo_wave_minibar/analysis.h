﻿#pragma once

#include <stdint.h>
#include <memory>
#include <vector>
#include <SDK/foobar2000.h>

struct waveform_entry
{
  uint16_t sample_count;
  std::vector<int16_t> min_samples;
  std::vector<int16_t> max_samples;
  std::vector<uint16_t> rms_samples;

  waveform_entry()
    : sample_count(0)
  {
  }
  explicit waveform_entry(uint16_t sample_count)
    : sample_count(sample_count)
    , min_samples(sample_count)
    , max_samples(sample_count)
    , rms_samples(sample_count)
  {
  }
  ~waveform_entry() {}
};

static void
downmix2(audio_sample* out, audio_sample const* in, size_t sample_count)
{
  for (size_t i = 0; i < sample_count; ++i) {
    out[i] = (in[0] + in[1]) / 2.0f;
    in += 2;
  }
}

static audio_chunk_impl
downmix_chunk(audio_chunk const& in)
{
  unsigned int channel_count = in.get_channels();
  if (channel_count == 1) {
    return in;
  }
  size_t sample_count = in.get_sample_count();
  std::vector<audio_sample> samples(sample_count);
  audio_sample const* data = in.get_data();
  if (channel_count == 2) {
    downmix2(samples.data(), data, sample_count);
  } else {
    for (size_t i = 0; i < sample_count; ++i) {
      audio_sample sample = 0.0f;
      for (size_t ch = 0; ch < channel_count; ++ch) {
        sample += data[ch];
      }
      samples[i] = sample / static_cast<float>(channel_count);
      data += channel_count;
    }
  }
  return audio_chunk_impl(
    samples.data(), sample_count, 1, in.get_sample_rate());
}

static int16_t
map_signed(float f)
{
  f = pfc::min_t(1.0f, pfc::max_t(-1.0f, f));
  float scale = static_cast<float>(f < 0.0f ? -SHRT_MIN : SHRT_MAX);
  int16_t ret = static_cast<int16_t>(f * scale);
  return ret;
}

static uint16_t
map_unsigned(float f)
{
  f = pfc::min_t(1.0f, f);
  float scale = static_cast<float>(USHRT_MAX);
  uint16_t ret = static_cast<uint16_t>(f * scale);
  return ret;
}

static float
unmap_signed(int16_t v)
{
  return v / static_cast<float>(v < 0 ? -SHRT_MIN : SHRT_MAX);
}

static float
unmap_unsigned(uint16_t v)
{
  return v / static_cast<float>(USHRT_MAX);
}

struct tic_toc
{
  uint64_t tic, freq;
  tic_toc()
  {
    QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
    QueryPerformanceCounter((LARGE_INTEGER*)&tic);
  }

  float measure()
  {
    uint64_t toc;
    QueryPerformanceCounter((LARGE_INTEGER*)&toc);
    return (toc - tic) / static_cast<float>(freq);
  }
};

struct song_slicer;

struct waveform_scan_context
{
  size_t slice_index;
  size_t slice_count;
  metadb_handle_ptr track;
  tic_toc tt;
  std::shared_ptr<song_slicer> song;
  std::shared_ptr<waveform_entry> wf;

  abort_callback& abort_cb;

  explicit waveform_scan_context(metadb_handle_ptr track, abort_callback& abort_cb);
  ~waveform_scan_context();

private:
  waveform_scan_context& operator=(waveform_scan_context const&);
  waveform_scan_context(waveform_scan_context const&);
};

std::shared_ptr<waveform_entry>
scan_waveform(waveform_scan_context& ctx);
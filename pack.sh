#!/usr/bin/env bash
[[ $# -eq 1 ]] || exit 1
version=$1
zip -9 -j foo_wave_minibar-${version}.fb2k-component portable/user-components/foo_wave_minibar/foo_wave_minibar.dll
zip -9 -j foo_wave_minibar-${version}-archive.fb2k-component portable/user-components/foo_wave_minibar/foo_wave_minibar.*
